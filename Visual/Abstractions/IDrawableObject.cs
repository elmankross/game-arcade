﻿using System;
using System.Drawing;

namespace Visual.Abstractions
{
    public interface IDrawableObject
    {
        /// <summary>
        /// Unique object id
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Object Area
        /// </summary>
        Rectangle Area { get; }

        /// <summary>
        /// Object skin
        /// </summary>
        ObjectSkin Skin { get; }
    }
}