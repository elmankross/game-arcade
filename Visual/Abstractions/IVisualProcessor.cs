﻿using System;

namespace Visual.Abstractions
{
    /// <summary>
    /// Отвечает за визуальную часть всего происходящего
    /// </summary>
    public interface IVisualProcessor : IDisposable
    {
        void DrawObject(IDrawableObject @object);
        void DrawParentChildObject(IDrawableObject parentObject, IDrawableObject childObject);
    }
}