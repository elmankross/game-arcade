﻿using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.IO;
using System.Text;
using Microsoft.Win32.SafeHandles;

namespace Visual.Console
{
    using Abstractions;
    using Skins;
    using Utils;

    public class ConsoleVisualProcessor : IVisualProcessor
    {
        /// <summary>
        /// Предыдущие положения объектов. Нужно для затирания "хвостов"
        /// </summary>
        private readonly ConcurrentDictionary<Guid, Rectangle> _prevObjectsPositions;

        /// <summary>
        /// 
        /// </summary>
        private readonly SafeFileHandle _consoleHandle;

        /// <summary>
        /// Набор скинов для объектов отрисовки
        /// </summary>
        private readonly ConcurrentDictionary<ObjectSkin, IConsoleSkin> _skins;


        public ConsoleVisualProcessor()
        {
            _prevObjectsPositions = new ConcurrentDictionary<Guid, Rectangle>();
            _consoleHandle = ConsoleApi.CreateFile("CONOUT$", 0x40000000, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);

            System.Console.CursorVisible = false;
            System.Console.InputEncoding = Encoding.GetEncoding("UTF-16");
            SetWindowSize(80, 50);

            _skins = new ConcurrentDictionary<ObjectSkin, IConsoleSkin>
            {
                [ObjectSkin.Default] = new DefaultSkin(),
                [ObjectSkin.Sword] = new SwordSkin(),
                [ObjectSkin.Warrior] = new WarriorSkin()
            };
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentObject"></param>
        /// <param name="childObject"></param>
        public void DrawParentChildObject(IDrawableObject parentObject, IDrawableObject childObject)
        {
            DrawBuffer(parentObject);

            if (childObject != null)
            {
                DrawBuffer(childObject);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="object"></param>
        public void DrawObject(IDrawableObject @object)
        {
            DrawParentChildObject(@object, null);
        }


        /// <summary>
        /// Отрисовывает элементы <see cref="IConsoleSkin"/> согласно расположениям
        /// </summary>
        /// <param name="object">Отрисовываемый объект</param>
        private void DrawBuffer(IDrawableObject @object)
        {
            if (!_skins.TryGetValue(@object.Skin, out var skin))
            {
                throw new Exception("Invalid Skin name!");
            }

            var buffer = new CharInfo[@object.Area.Height, @object.Area.Width];

            for (var height = 0; height < @object.Area.Height; height++)
            {
                for (var width = 0; width < @object.Area.Width; width++)
                {
                    CharInfo @char;

                    // Corner Left Top
                    if (height == 0 && width == 0)
                    {
                        @char = skin.Symbols[Position.CornerLeftTop];
                    }
                    // Corner Right Top
                    else if (height == 0 && width == (@object.Area.Width - 1))
                    {
                        @char = skin.Symbols[Position.CornerRightTop];
                    }
                    // Corner Left Bottom
                    else if (height == (@object.Area.Height - 1) && width == 0)
                    {
                        @char = skin.Symbols[Position.CornerLeftBottom];
                    }
                    // Corner Right Bottom
                    else if (height == (@object.Area.Height - 1) && width == (@object.Area.Width - 1))
                    {
                        @char = skin.Symbols[Position.CornerRightBottom];
                    }
                    // Border Horizontal
                    else if (height == 0 || height == (@object.Area.Height - 1))
                    {
                        @char = skin.Symbols[Position.BorderHorizontal];
                    }
                    // Border Vertical
                    else if (width == 0 || width == (@object.Area.Width - 1))
                    {
                        @char = skin.Symbols[Position.BorderVertical];
                    }
                    // Inner Space
                    else
                    {
                        @char = skin.Symbols[Position.InnerSpace];
                    }

                    buffer[height, width] = @char;
                }
            }

            CleanUpPreviousPosition(@object);
            WriteToBuffer(@object.Area, buffer);
        }


        /// <summary>
        /// Очистка "хвостов"
        /// </summary>
        /// <param name="object"></param>
        private void CleanUpPreviousPosition(IDrawableObject @object)
        {
            if (_prevObjectsPositions.TryGetValue(@object.Id, out var area))
            {
                var buffer = new CharInfo[area.Height, area.Width];
                for (var height = 0; height < area.Height; height++)
                {
                    for (var width = 0; width < area.Width; width++)
                    {
                        buffer[height, width] = _skins[ObjectSkin.Default].Symbols[Position.InnerSpace];
                    }
                }
                WriteToBuffer(area, buffer);
                _prevObjectsPositions.TryUpdate(@object.Id, @object.Area, area);
            }
            else
            {
                _prevObjectsPositions.TryAdd(@object.Id, @object.Area);
            }
        }


        /// <summary>
        /// Перерисовка области консоли
        /// </summary>
        /// <param name="objectArea">Область объекта отрисовки</param>
        /// <param name="buffer">Данные для отрисовки</param>
        private void WriteToBuffer(Rectangle objectArea, CharInfo[,] buffer)
        {
            var region = new SmallRect
            {
                Bottom = (short)objectArea.Bottom,
                Left = (short)objectArea.Left,
                Right = (short)objectArea.Right,
                Top = (short)objectArea.Top
            };

            ConsoleApi.WriteConsoleOutput(_consoleHandle, buffer,
                                          new Coord { X = (short)objectArea.Width, Y = (short)objectArea.Height },
                                          new Coord { X = 0, Y = 0 },
                                          ref region);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void SetWindowSize(int width, int height)
        {
            System.Console.SetWindowSize(width, height);
            System.Console.SetBufferSize(width, height);
        }


        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _skins.Clear();
            _consoleHandle.Close();
            _consoleHandle.Dispose();
        }
    }
}
