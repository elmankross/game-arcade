﻿using System;
using System.Runtime.InteropServices;

namespace Visual.Console.Utils
{
    [StructLayout(LayoutKind.Explicit)]
    public struct CharInfo
    {
        [FieldOffset(0)]
        public CharUnion Char;

        [FieldOffset(2)]
        public CharAttribute Attributes;
    }


    /// <summary>
    /// https://docs.microsoft.com/en-us/windows/console/char-info-str
    /// </summary>
    [Flags]
    public enum CharAttribute : short
    {
        ForegroundBlue = 0x0001,
        ForegroundGreen = 0x0002,
        ForegroundRed = 0x0004,
        ForegroundIntensity = 0x0008,
        BackgroundBlue = 0x0010,
        BackgroundGreen = 0x0020,
        BackgroundRed = 0x0040,
        BackgroundIntensity = 0x0080,
        CommonLvbLeadingByte = 0x0100,
        CommonLvbTrailingByte = 0x0200,
        //COMMON_LVB_GRID_HORIZONTAL = 0x0400,
        //COMMON_LVB_GRID_LVERTICAL = 0x0800,
        //COMMON_LVB_GRID_RVERTICAL = 0x1000,
        //COMMON_LVB_REVERSE_VIDEO = 0x4000,
        //COMMON_LVB_UNDERSCORE = 0x8000
    }
}
