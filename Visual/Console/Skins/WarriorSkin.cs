﻿namespace Visual.Console.Skins
{
    using Utils;

    public class WarriorSkin : DefaultSkin
    {
        internal WarriorSkin()
        {
            Symbols[Position.InnerSpace] = new CharInfo
            {
                Attributes = CharAttribute.ForegroundBlue,
                Char = new CharUnion { UnicodeChar = '\u2584' }
            };
        }
    }
}
