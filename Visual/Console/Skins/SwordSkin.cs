﻿using System.Collections.Generic;

namespace Visual.Console.Skins
{
    using Utils;

    public class SwordSkin : IConsoleSkin
    {
        public Dictionary<Position, CharInfo> Symbols { get; }

        internal SwordSkin()
        {
            Symbols = new Dictionary<Position, CharInfo>
            {
                [Position.InnerSpace] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundGreen,
                    Char = new CharUnion { UnicodeChar = '\u0000' }
                },
                [Position.CornerLeftTop] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundGreen,
                    Char = new CharUnion { UnicodeChar = '\u005E' }
                },
                [Position.CornerRightTop] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundGreen,
                    Char = new CharUnion { UnicodeChar = '\u005E' }
                },
                [Position.CornerLeftBottom] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundGreen,
                    Char = new CharUnion { UnicodeChar = '\u2020' }
                },
                [Position.CornerRightBottom] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundGreen,
                    Char = new CharUnion { UnicodeChar = '\u2020' }
                },
                [Position.BorderVertical] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundGreen,
                    Char = new CharUnion { UnicodeChar = '\u2502' }
                },
                [Position.BorderHorizontal] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundGreen,
                    Char = new CharUnion { UnicodeChar = '\u2502' }
                },
            };
        }
    }
}
