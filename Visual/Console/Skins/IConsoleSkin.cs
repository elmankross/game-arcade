﻿using System.Collections.Generic;

namespace Visual.Console.Skins
{
    using Utils;

    public enum Position
    {
        InnerSpace,
        CornerLeftTop,
        CornerRightTop,
        CornerLeftBottom,
        CornerRightBottom,
        BorderVertical,
        BorderHorizontal
    }

    public interface IConsoleSkin
    {
        Dictionary<Position, CharInfo> Symbols { get; }
    }
}