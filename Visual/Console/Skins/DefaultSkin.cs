﻿using System.Collections.Generic;

namespace Visual.Console.Skins
{
    using Utils;

    public class DefaultSkin : IConsoleSkin
    {
        public Dictionary<Position, CharInfo> Symbols { get; }

        internal DefaultSkin()
        {
            Symbols = new Dictionary<Position, CharInfo>
            {
                [Position.InnerSpace] = new CharInfo
                {
                    //Attributes = CharAttribute.ForegroundRed,
                    Char = new CharUnion { UnicodeChar = '\u0000' }
                },
                [Position.CornerLeftTop] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundRed,
                    Char = new CharUnion { UnicodeChar = '\u2554' }
                },
                [Position.CornerRightTop] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundRed,
                    Char = new CharUnion { UnicodeChar = '\u2557' }
                },
                [Position.CornerLeftBottom] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundRed,
                    Char = new CharUnion { UnicodeChar = '\u255A' }
                },
                [Position.CornerRightBottom] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundRed,
                    Char = new CharUnion { UnicodeChar = '\u255D' }
                },
                [Position.BorderVertical] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundRed,
                    Char = new CharUnion { UnicodeChar = '\u2551' }
                },
                [Position.BorderHorizontal] = new CharInfo
                {
                    Attributes = CharAttribute.ForegroundRed,
                    Char = new CharUnion { UnicodeChar = '\u2550' }
                },
            };
        }
    }
}
