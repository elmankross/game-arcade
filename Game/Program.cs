﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Visual.Console;

namespace Game
{
    static class Program
    {
        private static CancellationTokenRegistration CancellationTokenRegistration { get; set; }
        private static GameConfiguration Configuration { get; set; }
        private static Engine.Engine GameEngine { get; } = Engine.Engine.Instance;

        static void Main(string[] args)
        {
            CancellationTokenRegistration = new CancellationTokenRegistration();
            Configuration = new GameConfiguration();
            new ConfigurationBuilder().AddCommandLine(args).Build().Bind(Configuration);

            GameEngine.UseVisualProcessor<ConsoleVisualProcessor>();
            GameEngine.SetLevel(Configuration.Level);

            AppDomain.CurrentDomain.ProcessExit += (_, __) => CancellationTokenRegistration.Dispose();

            GameEngine.StartAsync(CancellationTokenRegistration.Token);

            while (!CancellationTokenRegistration.Token.IsCancellationRequested)
            {
                Thread.Sleep(300);
            }
        }
    }
}
