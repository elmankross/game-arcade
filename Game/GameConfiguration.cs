﻿using Engine.Levels;

namespace Game
{
    public class GameConfiguration
    {
        public string Level { get; set; }


        public GameConfiguration()
        {
            Level = NormalLevel.TITLE;
        }
    }
}
