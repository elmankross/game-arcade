﻿using System;
using System.Timers;

namespace Engine.Levels
{
    public class NormalLevel : ILevel
    {
        public const string TITLE = "Normal";

        public Timer Timer { get; }

        public NormalLevel()
        {
            Timer = new Timer(TimeSpan.FromMilliseconds(100).TotalMilliseconds);
        }
    }
}
