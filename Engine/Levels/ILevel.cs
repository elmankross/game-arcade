﻿using System.Timers;

namespace Engine.Levels
{
    public interface ILevel
    {
        Timer Timer { get; }
    }
}