﻿using Visual.Abstractions;

namespace Engine.Weapons
{
    public interface IWeapon : IDrawableObject
    {
        /// <summary>
        /// Урон
        /// </summary>
        decimal Damage { get; }

        /// <summary>
        /// Вес
        /// </summary>
        decimal Weight { get; }
    }
}