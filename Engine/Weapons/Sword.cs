﻿using System;
using System.Drawing;
using Visual;

namespace Engine.Weapons
{
    using Units;

    public class Sword : IWeapon
    {
        public Guid Id { get; }
        public Rectangle Area
        {
            get => _area;
            set => _area = value;
        }
        private Rectangle _area;

        public ObjectSkin Skin { get; }
        public decimal Damage { get; }
        public decimal Weight { get; }


        public Sword(IUnit unit)
        {
            Id = Guid.NewGuid();
            Area = new Rectangle(unit.Area.X + unit.Area.Width, unit.Area.Y, 1, 4);
            Skin = ObjectSkin.Sword;
            Damage = 100;
            Weight = 30;

            unit.Moved += (_, vector) => { _area.Offset((int)vector.X, (int)vector.Y); };
        }
    }
}
