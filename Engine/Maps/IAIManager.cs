﻿namespace Engine.Maps
{
    using Units;

    public interface IAIManager
    {
        void MoveUnit(ref IEnemy enemy);
    }
}