﻿using System;
using System.Collections.Generic;
using Visual.Abstractions;

namespace Engine.Maps
{
    using Goals;
    using Units;

    public interface IMap : IDrawableObject
    {
        /// <summary>
        /// Unique Map Name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Goal of map
        /// </summary>
        IGoal Goal { get; }

        /// <summary>
        /// Enemies
        /// </summary>
        IEnemy[] Enemies { get; }

        /// <summary>
        /// Not movable objects
        /// </summary>
        HashSet<IUnit> Environment { get; }

        /// <summary>
        /// 
        /// </summary>
        IAIManager AIManager { get; }

        /// <summary>
        /// Rise when enemy on map was been moved
        /// </summary>
        event EventHandler<IEnemy> EnemyMoved;

        /// <summary>
        /// 
        /// </summary>
        void MoveEnemies();
    }
}