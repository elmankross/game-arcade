﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Visual;

namespace Engine.Maps.CatchAngGo
{
    using Goals;
    using Units;

    public class CatchAndGoMap : IMap
    {
        public Guid Id { get; }
        public Rectangle Area { get; }
        public ObjectSkin Skin { get; }

        public string Name { get; }
        public IGoal Goal { get; }
        public IEnemy[] Enemies { get; }
        public HashSet<IUnit> Environment { get; }
        public IAIManager AIManager { get; }

        public event EventHandler<IEnemy> EnemyMoved;

        public CatchAndGoMap()
        {
            Id = Guid.NewGuid();
            Skin = ObjectSkin.Default;
            Name = nameof(CatchAndGoMap);
            Goal = new CaptureTheFlag();
            Area = Rectangle.FromLTRB(0, 0, 80, 40);
            Enemies = new IEnemy[]
            {
                new Warrior7x4(3, 6),
                new Warrior7x4(23, 6),
                new Warrior7x4(33, 6),
                new Warrior7x4(45, 6),
                new Warrior7x4(50, 6),
                new Warrior7x4(70, 6)
            };
            Environment = new HashSet<IUnit>();
            AIManager = new CatchAndGoAIManager(this);
        }


        public void MoveEnemies()
        {
            for (var i = 0; i < Enemies.Length; i++)
            {
                AIManager.MoveUnit(ref Enemies[i]);
                EnemyMoved?.Invoke(this, Enemies[i]);
            }
        }
    }
}
