﻿using System;
using System.Collections.Concurrent;
using System.Numerics;

namespace Engine.Maps.CatchAngGo
{
    using Units;

    internal class CatchAndGoAIManager : IAIManager
    {
        private readonly Random _random;
        private readonly ConcurrentDictionary<IEnemy, Vector2> _moveDirection;
        private readonly IMap _map;

        internal CatchAndGoAIManager(IMap map)
        {
            _map = map;
            _moveDirection = new ConcurrentDictionary<IEnemy, Vector2>();
            _random = new Random(DateTime.Now.Millisecond);
        }


        public void MoveUnit(ref IEnemy enemy)
        {
            _moveDirection.AddOrUpdate(enemy, new Vector2(_random.Next(-1, 1), _random.Next(-1, 1)),
                         (e, d) =>
                         {
                             if (d.X == 0 && d.Y == 0)
                             {
                                 return new Vector2(_random.Next(-1, 1), _random.Next(-1, 1));
                             }

                             e.MoveBy(d);
                             if (_map.Area.Contains(e.Area))
                             {
                                 return d;
                             }
                             else
                             {
                                 var dir = Vector2.Negate(d);
                                 e.MoveBy(dir);
                                 return dir;
                             }
                         });
        }
    }
}