﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Engine.Maps.CatchAngGo;
using Visual.Abstractions;

namespace Engine
{
    using Levels;
    using Maps;

    public class Engine : IDisposable
    {
        public event EventHandler<IMap> MapInitialized;
        public event EventHandler GameEnd;

        public IVisualProcessor VisualProcessor { get; private set; }

        private ILevel _level;
        private IMap _currentMap;
        private readonly Queue<IMap> _mapList;

        private static Engine _instance;

        private Engine()
        {
            _mapList = new Queue<IMap>(new[]
            {
                new CatchAndGoMap()
            });
        }


        /// <summary>
        /// Текущий инстанс игры
        /// </summary>
        public static Engine Instance => _instance ?? (_instance = new Engine());


        /// <summary>
        /// Запуск игры
        /// </summary>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            cancellationToken.Register(Stop);

            NextMap();
            _level.Timer.Start();

            return Task.CompletedTask;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void Stop()
        {
            _level.Timer.Stop();
        }


        /// <summary>
        /// Назначить уровень сложности игры
        /// </summary>
        /// <param name="level"></param>
        public void SetLevel(string level)
        {
            switch (level)
            {
                case NormalLevel.TITLE:
                    _level = new NormalLevel();
                    break;

                default:
                    throw new ArgumentOutOfRangeException(level);
            }

            _level.Timer.Elapsed += (_, __) => IterateAsync();
        }


        /// <summary>
        /// 
        /// </summary>
        public void UseVisualProcessor<TProcessor>()
            where TProcessor : class, IVisualProcessor, new()
        {
            VisualProcessor = new TProcessor();

            MapInitialized += (_, map) =>
            {
                VisualProcessor.DrawObject(map);
                foreach (var enemy in map.Enemies)
                {
                    VisualProcessor.DrawObject(enemy);
                }
            };
        }


        /// <summary>
        /// Включить следующую карту
        /// </summary>
        public void NextMap()
        {
            if (_mapList.TryDequeue(out var nextMap))
            {
                _currentMap = nextMap;
                MapInitialized?.Invoke(this, _currentMap);
                _currentMap.EnemyMoved += (_, enemy) => VisualProcessor?.DrawParentChildObject(enemy, enemy.Weapon);
            }
            else
            {
                GameEnd?.Invoke(this, null);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Task IterateAsync()
        {
            _currentMap.MoveEnemies();

            return Task.CompletedTask;
        }


        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            VisualProcessor.Dispose();
            _mapList.Clear();
        }
    }
}
