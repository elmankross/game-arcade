﻿using System;
using System.Drawing;
using System.Numerics;
using Visual;

namespace Engine.Units
{
    using Weapons;

    public class Warrior7x4 : IEnemy
    {
        public Guid Id { get; }

        public Rectangle Area
        {
            get => _area;
            set => _area = value;
        }
        private Rectangle _area;

        public ObjectSkin Skin { get; }
        public string Name { get; }
        public decimal Armor { get; }
        public decimal Health { get; }
        public IWeapon Weapon { get; }

        public event EventHandler<Vector2> Moved;

        public Warrior7x4(int x, int y)
        {
            Id = Guid.NewGuid();
            Skin = ObjectSkin.Warrior;
            Name = nameof(Warrior7x4);
            Armor = 100;
            Health = 100;
            Area = new Rectangle(x, y, 7, 4);
            Weapon = new Sword(this);
        }


        /// <inheritdoc />
        public void MoveBy(Vector2 vector)
        {
            _area.Offset((int)vector.X, (int)vector.Y);
            Moved?.Invoke(this, vector);
        }




        /// <inheritdoc />
        public void Fight(ref IUnit unit)
        {
            throw new System.NotImplementedException();
        }
    }
}
