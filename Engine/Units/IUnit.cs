﻿using System;
using System.Numerics;
using Visual.Abstractions;

namespace Engine.Units
{
    using Weapons;

    public interface IUnit : IDrawableObject
    {
        /// <summary>
        /// Имя юнита
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Броня
        /// </summary>
        decimal Armor { get; }

        /// <summary>
        /// Здоровье
        /// </summary>
        decimal Health { get; }

        /// <summary>
        /// Оружие
        /// </summary>
        IWeapon Weapon { get; }

        /// <summary>
        /// Move unit to new coordinates with steps
        /// </summary>
        /// <param name="vector"></param>
        void MoveBy(Vector2 vector);

        /// <summary>
        /// 
        /// </summary>
        event EventHandler<Vector2> Moved;
    }
}
