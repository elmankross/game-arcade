﻿namespace Engine.Units
{
    public interface IEnemy : IUnit
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unit"></param>
        void Fight(ref IUnit unit);
    }
}